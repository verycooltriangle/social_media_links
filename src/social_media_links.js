var patterns = {
  fb : {
    sure : {
      url : /likebox\.php\?[^"]*href=([^"&]*)/,
      html : /<[^>]*fb-like-box[^>]*data-href="([^"]*)[^>]*>/,
      xfbml : /<fb:like-box[^>]*href="([^"]*)/
    },
    maybe : /http(?:s)?:\/\/(?:www\.)?facebook\.com\/[a-zA-Z][^" &'\?]*/
  },
  twitter : {
    sure : {
        timeline : /twitter-timeline["][^"]*href="?([^"& ]*)/
    },
    maybe : /http(?:s)?:\/\/(?:www\.)?twitter\.com\/[a-zA-Z][^" &'\?]*/
  },
  yt : {
    sure : {
      subscribe : /g-ytsubscribe"?[^>]*data-channel="?([^"> ]+)/
    },
    maybe : /http(?:s)?:\/\/(?:www\.)?youtube\.com\/(?:c(?:hannel)?)?(?:u(?:ser)?)?\/[a-zA-Z][^" &'\?\n]*/
  },
  pinterest : {
    sure : {
      followButton : /data-pin-do="?buttonFollow"?[^>]*href="?([^" >]+)/,
      //will catch both embedboard and embeduser - board url will be with board name
      embed : /data-pin-do="?embed"?[^>]*href="?([^" >]+)/
    },
    maybe : /http(?:s)?:\/\/(?:www\.)?pinterest\.com\/(?!pin\/)[a-zA-Z][^" &'\?]*/
  }
};

function extractAddress(patterns) {
  return function (txt) {
    var i,
        key,
        result;

    if (patterns.sure) {
      for (key in patterns.sure) {
        if (patterns.hasOwnProperty(key)) {
          result = patterns[key].exec(txt);
          if (result) {
            return { found : unescape(result[1]) };
          }
        }
      }
    }

    if (patterns.maybe) {
      result = txt.match(patterns.maybe);
      if (result) {
        return { maybe : result };
      }
    }

    return {};
  };
}

module.exports = {
  fromPattern : extractAddress,
  fb : extractAddress(patterns.fb),
  twitter : extractAddress(patterns.twitter),
  yt : extractAddress(patterns.yt),
  pinterest : extractAddress(patterns.pinterest)
};
