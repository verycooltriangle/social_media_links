var GSS = require('google-spreadsheet'),
    http = require('http'),
    social = require('./social_media_links'),
    count = 0,
    credentials = {
      u : '',
      p : ''
    };


(function getLinksFromSpreadSheet(key) {
  var sheet = new GSS(key);

  sheet.setAuth(credentials.u, credentials.p, function (err) {
    sheet.getRows(1, function (err, rowData) {

      var rows = rowData.filter(function (v) {
        return v.title.indexOf('facebook.com') < 0;
      });

      processRows(rows);
    });
  });
}('11kk2yxjfqRQ35mfUZG_fe-rB2frm59IDvGgwgx0EL4A'));


function processRows (rows) {
  rows.forEach(function(row, i, arr) {
    var link = row.title.replace('https', 'http');
    if (link.indexOf('http') !== 0) {
      link = 'http://' + link;
    }
    var request = http.get(link, function (response) {
      var page = '';

      response.on('error', function (e) {

      });

      response.on('data', function (chunk) {
        page += chunk.toString();
      });

      response.on('end', function () {
        var fb = social.fb(page);
        updateRow(social.fb(page), row, 'fb');
        updateRow(social.twitter(page), row, 'twitter');
        updateRow(social.yt(page), row, 'yt');
        updateRow(social.pinterest(page), row, 'pinterest');

        row.save();

        count++;
        if (count === arr.length) console.log('finished');
      });
    });

    request.on('error', function (e) {
      count++;
      row.fb = e.errno;
      row.save();
      console.log(row.title, e);
    });
  });
}

function updateRow(link, row, rowTitle) {
  if (link.found) {
    row[rowTitle] = link.found;
  } else if (link.maybe) {
    row[rowTitle] = link.maybe.join('\n');
  } else {
    row[rowTitle] = '';
  }
}
